use adw::subclass::prelude::*;
use gtk::glib;

mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct Player {
        pub player: gstreamer_play::Play,
        // Just keeping this around here so it can emit signals.
        #[allow(dead_code)]
        signal_adapter: gstreamer_play::PlaySignalAdapter,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Player {
        const NAME: &'static str = "Player";
        type Type = super::Player;

        fn new() -> Self {
            let player = gstreamer_play::Play::new(None::<gstreamer_play::PlayVideoRenderer>);
            let signal_adapter = gstreamer_play::PlaySignalAdapter::new(&player);

            signal_adapter.connect_error(|_player, err, _details| {
                log::error!("Playback error: {err}");
            });

            Self {
                player,
                signal_adapter,
            }
        }
    }

    impl ObjectImpl for Player {}

    impl Drop for Player {
        fn drop(&mut self) {
            self.player.message_bus().set_flushing(true);
        }
    }
}

glib::wrapper! {
    pub struct Player(ObjectSubclass<imp::Player>);
}

impl Player {
    pub fn play(&self, romaji: String) {
        let imp = self.imp();
        imp.player.set_uri(Some(&format!(
            "resource:///com/felipekinoshita/Kana/audio/{}.wav",
            romaji.to_lowercase()
        )));
        imp.player.play();
    }

    pub fn stop(&self) {
        let imp = self.imp();
        imp.player.stop();
    }
}

impl Default for Player {
    fn default() -> Self {
        glib::Object::new()
    }
}
