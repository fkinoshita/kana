<?xml version="1.0" encoding="UTF-8"?>
<component type="desktop-application">
  <id>@app-id@</id>
  <metadata_license>CC0-1.0</metadata_license>
  <project_license>GPL-3.0-or-later</project_license>
  <name>Kana</name>
  <developer_name>Felipe Kinoshita</developer_name>
  <summary>Learn Japanese characters</summary>
  <description>
    <p>
      Hone your Japanese skills by matching romanized characters to their correct hiragana and katakana counterparts.
	  </p>
  </description>
  <translation type="gettext">@gettext-package@</translation>
  <keywords>
    <keyword>japanese</keyword>
    <keyword>hiragana</keyword>
    <keyword>katakana</keyword>
    <keyword>kana</keyword>
  </keywords>
  <requires>
    <internet>offline-only</internet>
    <display_length compare="ge">360</display_length>
  </requires>
  <recommends>
    <control>pointing</control>
    <control>keyboard</control>
    <control>touch</control>
  </recommends>
  <screenshots>
    <screenshot type="default">
      <image>https://gitlab.gnome.org/fkinoshita/Kana/-/raw/main/data/screenshots/welcome.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/fkinoshita/Kana/-/raw/main/data/screenshots/light.png</image>
    </screenshot>
    <screenshot>
      <image>https://gitlab.gnome.org/fkinoshita/Kana/-/raw/main/data/screenshots/dark.png</image>
    </screenshot>
  </screenshots>
  <launchable type="desktop-id">@app-id@.desktop</launchable>
  <url type="homepage">https://gitlab.gnome.org/fkinoshita/Kana</url>
  <url type="bugtracker">https://gitlab.gnome.org/fkinoshita/Kana/-/issues</url>
  <url type="contribute">https://gitlab.gnome.org/fkinoshita/Kana</url>
  <url type="donation">https://ko-fi.com/fkinoshita</url>
  <url type="vcs-browser">https://gitlab.gnome.org/fkinoshita/Kana</url>
  <url type="contact">https://matrix.to/#/@fkinoshita:gnome.org</url>
  <update_contact>fkinoshita_AT_gnome.org</update_contact>
  <content_rating type="oars-1.1"/>
  <!-- <custom> -->
    <!-- <value key="GnomeSoftware::key-colors">[(0, 0, 0)]</value> -->
  <!-- </custom> -->
<releases>
    <release date="2023-12-01" version="1.4">
      <description translatable="no">
        <p>Small bugfix release:</p>
        <ul>
          <li>Fix some guesses not getting highlighted</li>
        </ul>
        <p>If you would like to come with suggestions, report bugs, translate the app, or contribute otherwise, feel free to reach out!</p>
      </description>
    </release>
    <release date="2023-11-25" version="1.3">
      <description translatable="no">
        <p>Small bugfix release with a new feature:</p>
        <ul>
          <li>Now when entering an incorrect guess, the correct guess will be highlighted</li>
          <li>Small quality-of-life tweaks to improve user experience</li>
          <li>Prevent memory leaks from audio player</li>
        </ul>
        <p>If you would like to come with suggestions, report bugs, translate the app, or contribute otherwise, feel free to reach out!</p>
      </description>
    </release>
    <release date="2023-11-07" version="1.2">
      <description translatable="no">
        <p>Bite-sized release:</p>
        <ul>
          <li>Make play sound button fixed in place</li>
          <li>Add setting to play character sound automatically</li>
        </ul>
        <p>If you would like to come with suggestions, report bugs, translate the app, or contribute otherwise, feel free to reach out!</p>
      </description>
    </release>
    <release date="2023-11-05" version="1.1">
      <description translatable="no">
        <p>Small bugfix release:</p>
        <ul>
          <li>Only make play sound shortcut work on the main page</li>
        </ul>
        <p>If you would like to come with suggestions, report bugs, translate the app, or contribute otherwise, feel free to reach out!</p>
      </description>
    </release>
    <release date="2023-11-01" version="1.0">
      <description translatable="no">
        <p>Initial release.</p>
      </description>
    </release>
  </releases>
</component>
