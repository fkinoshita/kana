<h1 align="center">
  <img src="data/icons/hicolor/scalable/apps/com.felipekinoshita.Kana.svg" alt="Kana App Icon" width="192" height="192"/>
  <br>
  Kana
</h1>

<p align="center"><strong>Learn Japanese characters</strong></p>

<p align="center">
  <a href="https://flathub.org/apps/details/com.felipekinoshita.Kana">
    <img width="200" alt="Download on Flathub" src="https://flathub.org/assets/badges/flathub-badge-en.svg"/>
  </a>
  <br>
</p>

<p align="center">
  <a href="https://flathub.org/apps/details/com.felipekinoshita.Kana">
    <img alt="Flathub downloads" src="https://img.shields.io/badge/dynamic/json?color=informational&label=downloads&logo=flathub&logoColor=white&query=%24.installs_total&url=https%3A%2F%2Fflathub.org%2Fapi%2Fv2%2Fstats%2Fcom.felipekinoshita.Kana"/>
  </a>
</p>

<p align="center">
  <img src="/data/screenshots/welcome.png" alt="Preview of Kana app window"/>
  <img src="/data/screenshots/light.png" alt="Preview of Kana app window in light mode"/>
  <img src="/data/screenshots/dark.png" alt="Previous of Kana app window in dark mode"/>
</p>

Hone your Japanese skills by matching romanized characters to their correct hiragana and katakana counterparts.

<!-- ## Translation -->

## Code of conduct

Kana follows the GNOME project [Code of Conduct](./code-of-conduct.md). All
communications in project spaces, such as the issue tracker are expected to follow it.

